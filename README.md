# Dal Personal Website

Last Updated: 13 September, 2019

This website is hosted on Dalhousie University's Hector Server for Graduate Students. The link is [here](https://web.cs.dal.ca/~mmali/).

The design of this website has been taken from [CSS Tricks](https://css-tricks.com/one-page-resume-site/) and [Ariana Mirian](https://arianamirian.com/).
